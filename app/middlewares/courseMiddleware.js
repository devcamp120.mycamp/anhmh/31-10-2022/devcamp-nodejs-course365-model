const printCourseMiddleware = (request, response, next) => {
    console.log("Course URL: " + request.url);
    next();
}
module.exports = {
    printCourseMiddleware: printCourseMiddleware
}