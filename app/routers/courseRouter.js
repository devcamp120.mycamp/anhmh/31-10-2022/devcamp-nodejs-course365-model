//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import Course Middleware
const { printCourseMiddleware } = require("../middlewares/courseMiddleware");
//Import module course controllers
const { getAllCourse, getCourseById, createCourse, updateCourseById, deleteCourseById  } = require('../controllers/courseController');
const courseRouter = express.Router();

//Get a course
courseRouter.get("/courses", printCourseMiddleware, getAllCourse);

//Create a course
courseRouter.post("/courses", printCourseMiddleware, createCourse);

//Get a course by id
courseRouter.get("/courses/:courseId", printCourseMiddleware, getCourseById);

//Update a course by id
courseRouter.put("/courses/:courseId", printCourseMiddleware, updateCourseById);

//Delete a course by id
courseRouter.delete("/courses/:courseId", printCourseMiddleware, deleteCourseById);

//Export dữ liệu thành 1 module
module.exports = courseRouter;