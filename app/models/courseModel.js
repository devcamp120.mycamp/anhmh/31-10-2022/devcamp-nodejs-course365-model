//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const courseSchema = new Schema({
    __id: {
        type: mongoose.Types.ObjectId
    },
    courseCode: {
        type: String,
        unique: true,
        required: true
    },
    courseName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    discountPrice: {
        type: Number,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    coverImage: {
        type: String,
        required: true
    },
    teacherName: {
        type: String,
        required: true
    },
    teacherPhoto: {
        type: String,
        required: true
    },
    isPopular: {
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

//Bước 4: Export ra 1 model cho Schema
module.exports = mongoose.model("Course", courseSchema);