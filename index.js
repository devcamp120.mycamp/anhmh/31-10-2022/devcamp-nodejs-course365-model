//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");

//Khai báo thư viện Mongoose
const mongoose = require('mongoose');

//Import Router sử dụng Middelwares
const courseRouter = require("./app/routers/courseRouter");

//Khai báo các model 
const courseModel = require("./app/models/courseModel");

//Kết nối đến cơ sở dữ liệu Mongodb
mongoose.connect('mongodb://localhost:27017/CRUD_Course365', (error) => {
    if(error) {
        throw error;
    }
    console.log("Successfully connected!");
});

//Khởi tạo app express
const app = new express();

//Sử dụng được body json
app.use(express.json());

//Sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//Khai báo cổng của project 
const port = 8000;

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
})
//Khai báo sử dụng các tài nguyên static (images, js, css, v...v...)
app.use(express.static("views"));

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/course365", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + '/views/course365.html'));
})

app.use("/", courseRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})